//
//  FileUtility.m
//  OpenAllVideoFormat
//
//  Created by truonghm on 3/1/14.
//  Copyright (c) 2014 Hoang Manh Truong. All rights reserved.
//

#import "FileUtility.h"

@implementation FileUtility

#pragma mark - global path
+ (NSString *)cacheFolderPath{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}
+ (NSString *)documentFolderPath{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

#pragma mark - global action
+ (BOOL)removeFileAtPath:(NSString *)path{
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
    if (error) {
        NSLog(@"can not remove file %@ error %@",path, error.localizedDescription);
        return NO;
    }
    return YES;
}
+ (BOOL)removeFolderAtPath:(NSString *)folder{
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:folder error:&error];
    if (error) {
        NSLog(@"can not remove folder %@ error %@",folder, error.localizedDescription);
        return NO;
    }
    return YES;
}
+ (BOOL)writeData:(NSData *)data toPath:(NSString *)path{
    BOOL success = [data writeToFile:path atomically:YES];
    if (!success) {
        NSLog(@"error write data to path %@",path);
        return NO;
    }
    return YES;
}
+ (BOOL)createFolderAtPath:(NSString *)path{
    NSError *error = nil;
    NSFileManager  *fileManager = [NSFileManager defaultManager];
    BOOL success = [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
    if (!success) {
        NSLog(@"can not create folder, error %@",error.localizedDescription);
        return NO;
    }
    return YES;
}
+ (NSArray *)pathFilesAtFolder:(NSString *)folder{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager subpathsAtPath:folder];
}
#pragma mark - for image
+ (BOOL)writeImage:(UIImage *)image fileAtPath:(NSString *)path compressionQuality:(CGFloat)quality{
    NSData *data = UIImageJPEGRepresentation(image, quality);
    if (!data) {
        NSLog(@"image is nil");
        return NO;
    }
    return [self writeData:data toPath:path];
}
@end
