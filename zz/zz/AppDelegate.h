//
//  AppDelegate.h
//  zz
//
//  Created by truonghm on 4/9/14.
//  Copyright (c) 2014 truonghm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
