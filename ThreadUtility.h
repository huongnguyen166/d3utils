//
//  ThreadUtility.h
//  OpenAllVideoFormat
//
//  Created by truonghm on 3/18/14.
//  Copyright (c) 2014 Hoang Manh Truong. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DependencyOperationQueue;

@interface ThreadUtility : NSObject

+ (DependencyOperationQueue *)shareDependencyQueue1;
+ (DependencyOperationQueue *)shareDependencyQueue2;
+ (DependencyOperationQueue *)shareDependencyQueue3;
+ (DependencyOperationQueue *)shareDependencyQueue4;

@end


@interface DependencyOperationQueue : NSOperationQueue

- (void)addExecuteBlock:(void(^)())block;

@end