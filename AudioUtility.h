//
//  AudioUtility.h
//  ArtFile
//
//  Created by truonghm on 3/18/14.
//  Copyright (c) 2014 truonghm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioUtility : NSObject
+ (AudioUtility *)shareInstance;

- (void)artImageFromSoundURL:(NSURL *)fileURL success:(void(^)(UIImage *image))success;

@end
