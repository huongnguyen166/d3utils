//
//  FileUtility.h
//  OpenAllVideoFormat
//
//  Created by truonghm on 3/1/14.
//  Copyright (c) 2014 Hoang Manh Truong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUtility : NSObject

#pragma mark - global path
/* Trả về đường dẫn tới thư mục cache */
+ (NSString *)cacheFolderPath;
/* Trả về đường dẫn tới thư mục document */
+ (NSString *)documentFolderPath;

#pragma mark - global action
/* Xoá file, input là đường dẫn, output là việc xoá có thành công hay không */
+ (BOOL)removeFileAtPath:(NSString *)path;
/* Xoá folder, input là đường dẫn, output là việc xoá có thành công hay không */
+ (BOOL)removeFolderAtPath:(NSString *)folder;
/* Ghi dữ liệu vào một đường dẫn, input là dữ liệu cần viết, đường dẫn cần ghi, output là việc ghi dữ liệu có thanh công hay không*/
+ (BOOL)writeData:(NSData *)data toPath:(NSString *)path;
/* Tạo folder, input là đường dẫn folder, output là việc tạo folder có thành công hay không*/
+ (BOOL)createFolderAtPath:(NSString *)path;
/* Lấy ra danh sách file của folder, input là đường dẫn folder, ouput là đường dẫn các file của folder */
+ (NSArray *)pathFilesAtFolder:(NSString *)folder;
#pragma mark - for image
/* Ghi ảnh vào một đường dẫn, input là đường dẫn, chất lượng nén, và ảnh. Output là việc ghi file có thành công hay không */
+ (BOOL)writeImage:(UIImage *)image fileAtPath:(NSString *)path compressionQuality:(CGFloat)quality;
@end
