//
//  ThreadUtility.m
//  OpenAllVideoFormat
//
//  Created by truonghm on 3/18/14.
//  Copyright (c) 2014 Hoang Manh Truong. All rights reserved.
//

#import "ThreadUtility.h"

static DependencyOperationQueue *shareDependencyQueue1;
static DependencyOperationQueue *shareDependencyQueue2;
static DependencyOperationQueue *shareDependencyQueue3;
static DependencyOperationQueue *shareDependencyQueue4;

@implementation ThreadUtility

+ (DependencyOperationQueue *)shareDependencyQueue1{
    if (!shareDependencyQueue1) {
        shareDependencyQueue1 = [[DependencyOperationQueue alloc] init];
    }
    return shareDependencyQueue1;
}
+ (DependencyOperationQueue *)shareDependencyQueue2{
    if (!shareDependencyQueue2) {
        shareDependencyQueue2 = [[DependencyOperationQueue alloc] init];
    }
    return shareDependencyQueue2;
}
+ (DependencyOperationQueue *)shareDependencyQueue3{
    if (!shareDependencyQueue3) {
        shareDependencyQueue3 = [[DependencyOperationQueue alloc] init];
    }
    return shareDependencyQueue3;
}
+ (DependencyOperationQueue *)shareDependencyQueue4{
    if (!shareDependencyQueue4) {
        shareDependencyQueue4 = [[DependencyOperationQueue alloc] init];
    }
    return shareDependencyQueue4;
}

@end

@implementation DependencyOperationQueue

-(void)addOperation:(NSOperation *)op {
    NSOperation *lastOperation = [self.operations lastObject];
    if (lastOperation) {
        [op addDependency:lastOperation];
    }
    [super addOperation:op];
}

- (void)addExecuteBlock:(void(^)())block{
    NSBlockOperation *operation = [[NSBlockOperation alloc] init];
    [operation addExecutionBlock:block];
    [self addOperation:operation];
}
@end