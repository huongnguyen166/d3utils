//
//  Macro.h
//  iOSUtility
//
//  Created by TruongHM on 2/21/14.
//  Copyright (c) 2014 DIS. All rights reserved.
//

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)

#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)


#define RGBCOLOR(RED, GREEN, BLUE, ALPHA) [UIColor colorWithRed:RED/255.0f green:GREEN/255.0f blue:BLUE/255.0f alpha:ALPHA]

#define OBJECT_NULL  (id)[NSNull null]


#define IS_OS7      ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)

#define IS_IPAD ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)

// Core data store
#define CORE_DATA_STORE_NAME @"prism.sqlite"
#define kAllObjectDidSaveToLocalNotification @"kAllObjectDidSaveToLocalNotification"

// NSUserdefault key
// Last time sync - each object
#define kSyncEquiupmentLastTime @"kSyncEquiupmentLastTime"
//#define kSyncProductLastTime @"kSyncProductLastTime"
//#define kSyncManufacturerLastTime @"kSyncManufacturerLastTime"
//#define kSyncEquipmentHistoryLastTime @"kSyncEquipmentHistoryLastTime"

// debug sqlite core data
#define kDebugingCoredata @"com.apple.CoreData.SQLDebug"

