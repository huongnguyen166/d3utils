//
//  AudioUtility.m
//  ArtFile
//
//  Created by truonghm on 3/18/14.
//  Copyright (c) 2014 truonghm. All rights reserved.
//

#import "AudioUtility.h"

static AudioUtility *shareInstance;

@implementation AudioUtility

+ (AudioUtility *)shareInstance{
    if (!shareInstance) {
        shareInstance = [[AudioUtility alloc] init];
    }
    return shareInstance;
}

- (void)artImageFromSoundURL:(NSURL *)fileURL success:(void(^)(UIImage *image))success{
    NSLog(@"start get art image from url %@",fileURL.path);
    AVAsset *asset = [AVURLAsset URLAssetWithURL:fileURL options:nil];
    NSLog(@"start get art image with asset %@",asset);
    NSArray *keys = [NSArray arrayWithObjects:@"commonMetadata", nil];
    [asset loadValuesAsynchronouslyForKeys:keys completionHandler:^{
        NSArray *artworks = [AVMetadataItem metadataItemsFromArray:asset.commonMetadata
                                                           withKey:AVMetadataCommonKeyArtwork
                                                          keySpace:AVMetadataKeySpaceCommon];
        for (AVMetadataItem *item in artworks) {
            UIImage *image = nil;
            if ([item.keySpace isEqualToString:AVMetadataKeySpaceID3]) {
                NSDictionary *dict = [item.value copyWithZone:nil];
                image = [UIImage imageWithData:[dict objectForKey:@"data"]];
            } else if ([item.keySpace isEqualToString:AVMetadataKeySpaceiTunes]) {
                image = [UIImage imageWithData:[item.value copyWithZone:nil]];
            }
            
            if (image) {
                CGSize newSize = CGSizeMake(272., 204);
                UIGraphicsBeginImageContext(newSize);
                [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
                UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
                success(newImage);
            }
        }
    }];
}

@end
